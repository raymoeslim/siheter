<section class="content-header">
    <h1>
        Data Kelahiran Hewan Ternak
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-heartbeat"></i>Data Kelahiran Hewan Ternak</a></li>

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=kelahiran/add">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                    <a class="btn btn-app" href="kelahiran/print.php" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-striped table-condensed table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID Kelahiran</th>
                                <th>ID Hewan</th>
                                <th>ID Ibu</th>
                                <th>ID Bapa</th>
                                <th>Jenis Sapi</th>
                                <th>Tanggal Lahir</th>
                                <th>Bobot</th>
                                <th>Tinggi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 1;
                            $query = mysqli_query($config, "select * from kelahiran");
                            while ($data = mysqli_fetch_array($query)) {
                                echo "<tr>
                                        <td>$i</td>
                                        <td>$data[id_kelahiran]</td>
                                        <td>$data[id_hewan]</td>
                                        <td>$data[id_ibu]</td>
                                        <td>$data[id_bapa]</td>
                                        <td>$data[jenis_sapi]</td>
                                        <td>$data[tgl_lahir]</td>
                                        <td>$data[bobot] Kg.</td>
                                        <td>$data[tinggi] cm</td>
                                        <td>
                                            <a href='index.php?content=kelahiran/delete&id_kelahiran=$data[id_kelahiran]' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</a>
                                            <a href='index.php?content=kelahiran/edit&id_kelahiran=$data[id_kelahiran]' class='btn btn-info'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        
                                        </td>
                                    </tr>";
                                $i = $i + 1;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->