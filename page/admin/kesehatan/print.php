<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html moznomarginboxes mozdisallowselectionprint>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="sourcut icon" href="../../../../../../../assets/dist/img/logo.png">
    <script language="javascript1.2">
        function printpage() {
            window.print();
        }
    </script>
</head>

<body onload="printpage()">
    <div style="color: #000;" align="center">
        <img src="../../../assets/dist/img/logo.png" alt="logo" height="100">
        <h3>Sistem Informasi Hewan Ternak Sapi</h3>
        <p><strong>UPTD BPPPT Majalengka</strong></p>
    </div>
    <table border="1" align="center" style="border-collapse:collapse;">
        <th colspan="8" align="center">Data Kesehatan Hewan Ternak</th>
        <tr>
            <th>No</th>
            <th>ID Kesehatan</th>
            <th>ID Hewan</th>
            <th>Tanggal Periksa</th>
            <th>Gejala</th>
            <th>Jenis Obat</th>
            <th>Jumlah</th>
            <th>Jadwal Check Up</th>
        </tr>
        <?php
        include "../../../config/connection.php";
        $i = 1;
        $query = mysqli_query($config, "select * from kesehatan");
        while ($data = mysqli_fetch_array($query)) {
            echo "<tr>
                	<td>$i.</td>
                    <td>$data[id_kesehatan]</td>
                    <td>$data[id_hewan]</td>
                    <td>$data[tgl_periksa]</td>
                    <td>$data[gejala]</td>
                    <td>$data[jenis_obat]</td>
                    <td>$data[cc] Cc.</td>
                    <td>$data[jadwal_cek_up]</td>
                  </tr>";
            $i = $i + 1;
        }
        ?>
    </table>
</body>

</html>