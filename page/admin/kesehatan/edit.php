<section class="content-header">
    <h1>
        Edit Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-edit"></i>Edit Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=kesehatan/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <form action="index.php?content=kesehatan/update" method="post" enctype="multipart/form-data">
                        <h3>Edit Data Kesehatan Hewan Ternak</h3>

                        <?php
                        $hasil = $_REQUEST['id_kesehatan'];
                        $data = mysqli_fetch_array(mysqli_query($config, "select * from kesehatan where id_kesehatan='$hasil'"));
                        ?>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID Kesehatan</th>
                                <td width="1%">:</td>
                                <td>

                                    <input type="text" class="form-control" value="<?php echo $data['id_kesehatan']; ?>" name="id_kesehatan" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th>ID Hewan</th>
                                <td>:</td>
                                <td>
                                    <input type="text" class="form-control" value="<?php echo $data['id_hewan']; ?>" name="id_hewan" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Periksa</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" value="<?php echo $data['tgl_periksa']; ?>" name="tgl_periksa" required></td>
                            </tr>
                            <tr>
                                <th>Gejala</th>
                                <td>:</td>
                                <td><textarea class="form-control" name="gejala" required><?php echo $data['gejala']; ?></textarea></td>
                            </tr>
                            <tr>
                                <th width="20%">Jenis Obat</th>
                                <td width="1%">:</td>
                                <td><input type="text" class="form-control" name="jenis_obat" value="<?php echo $data['jenis_obat']; ?>" required onkeyup="validHuruf(this)"></td>
                            </tr>
                            <tr>
                                <th>Jumlah</th>
                                <td>:</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" name="cc" required onkeyup="validAngka(this)" value="<?php echo $data['cc']; ?>" class="form-control">
                                        <span class="input-group-addon">Cc.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Check Up</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" name="jadwal_cek_up" value="<?php echo $data['jadwal_cek_up']; ?>" required></td>
                            </tr>

                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Update
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->