<section class="content-header">
    <h1>
        Data Mutasi Hewan Ternak
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-random"></i>Data Mutasi Hewan Ternak</a></li>

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=mutasi/add">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                    <a class="btn btn-app" href="mutasi/print.php" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-striped table-condensed table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID Mutasi</th>
                                <th>ID Hewan</th>
                                <th>Tanggal Mutasi</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 1;
                            $query = mysqli_query($config, "select * from mutasi");
                            while ($data = mysqli_fetch_array($query)) {
                                echo "<tr>
                                        <td>$i</td>
                                        <td>$data[id_mutasi]</td>
                                        <td>$data[id_hewan]</td>
                                        <td>$data[tgl]</td>
                                        <td>$data[status]</td>
                                        <td>$data[ket]</td>
                                        <td><img src='../../assets/images/mutasi/$data[foto]' class='foto'/></td>
                                        <td>
                                            <a href='index.php?content=mutasi/delete&id_mutasi=$data[id_mutasi]' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</a>
                                            <a href='index.php?content=mutasi/edit&id_mutasi=$data[id_mutasi]' class='btn btn-info'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        
                                        </td>
                                    </tr>";
                                $i = $i + 1;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->