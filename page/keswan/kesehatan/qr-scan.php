<section class="content-header">
  <h1>
    Data Kesehatan Hewan Ternak
    <small>Sistem Informasi Hewan Ternak Sapi</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#"><i class="fa fa-medkit"></i>Data Kesehatan Hewan Ternak</a></li>

  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">

          <a class="btn btn-app" href="index.php?content=kesehatan/get">
            <i class="fa fa-reply"></i> Kembali
          </a>
          <a class="btn btn-app" href="">
            <i class="fa fa-refresh"></i> Refresh
          </a>
        </div>
      </div>

      <!-- /.box -->
      <div class="box">
        <div class="box-header">

        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <center>
            <video id="previewKamera" style="width:100%;"></video>
          </center>
          <br>
          <select id="pilihKamera" style="max-width:400px">
          </select>
          <br>
          <br>
          <a href="" id="hasilscan" class='btn btn-info hidden'><i class='fa fa-plus'></i> Tambah Kesehatan Hewan</a>
          <!-- <input type="text" id="hasilscan"> -->

        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

<script>
  let selectedDeviceId = null;
  const codeReader = new ZXing.BrowserMultiFormatReader();
  const sourceSelect = $("#pilihKamera");

  $(document).on('change', '#pilihKamera', function() {
    selectedDeviceId = $(this).val();
    if (codeReader) {
      codeReader.reset()
      initScanner()
    }
  })

  function initScanner() {
    codeReader
      .listVideoInputDevices()
      .then(videoInputDevices => {
        videoInputDevices.forEach(device =>
          console.log(`${device.label}, ${device.deviceId}`)
        );

        if (videoInputDevices.length > 0) {

          if (selectedDeviceId == null) {
            if (videoInputDevices.length > 1) {
              selectedDeviceId = videoInputDevices[1].deviceId
            } else {
              selectedDeviceId = videoInputDevices[0].deviceId
            }
          }


          if (videoInputDevices.length >= 1) {
            sourceSelect.html('');
            videoInputDevices.forEach((element) => {
              const sourceOption = document.createElement('option')
              sourceOption.text = element.label
              sourceOption.value = element.deviceId
              if (element.deviceId == selectedDeviceId) {
                sourceOption.selected = 'selected';
              }
              sourceSelect.append(sourceOption)
            })

          }

          codeReader
            .decodeOnceFromVideoDevice(selectedDeviceId, 'previewKamera')
            .then(result => {

              //hasil scan

              // $("#hasilscan").attr("href", atob(result.text));
              // $("#hasilscan").removeClass("hidden");
              location.replace(atob(result.text));

              if (codeReader) {
                codeReader.reset()
              }
            })
            .catch(err => console.error(err));

        } else {
          alert("Camera not found!")
        }
      })
      .catch(err => console.error(err));
  }


  if (navigator.mediaDevices) {


    initScanner()


  } else {
    alert('Cannot access camera.');
  }
</script>