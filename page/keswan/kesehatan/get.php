<section class="content-header">
    <h1>
        Data Kesehatan Hewan Ternak
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-medkit"></i>Data Kesehatan Hewan Ternak</a></li>

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=kesehatan/add">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                    <a class="btn btn-app" href="kesehatan/print.php" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                    <a class="btn btn-app" href="index.php?content=kesehatan/qr-scan">
                        <i class="fa fa-qrcode"></i>Scan QR Code
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-striped table-condensed table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID Kesehatan</th>
                                <th>ID Hewan</th>
                                <th>Tanggal Periksa</th>
                                <th>Gejala</th>
                                <th>Jenis Obat</th>
                                <th>Jumlah</th>
                                <th>Jadwal Check Up</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 1;
                            $query = mysqli_query($config, "select * from kesehatan");
                            while ($data = mysqli_fetch_array($query)) {
                                echo "<tr>
                                        <td>$i</td>
                                        <td>$data[id_kesehatan]</td>
                                        <td>$data[id_hewan]</td>
                                        <td>$data[tgl_periksa]</td>
                                        <td>$data[gejala]</td>
                                        <td>$data[jenis_obat]</td>
                                        <td>$data[cc] Cc.</td>
                                        <td>$data[jadwal_cek_up]</td>
                                        <td>
                                            <a href='index.php?content=kesehatan/delete&id_kesehatan=$data[id_kesehatan]' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</a>
                                            <a href='index.php?content=kesehatan/edit&id_kesehatan=$data[id_kesehatan]' class='btn btn-info'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        
                                        </td>
                                    </tr>";
                                $i = $i + 1;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->