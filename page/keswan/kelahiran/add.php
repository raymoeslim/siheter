<section class="content-header">
    <h1>
        Tambah Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-plus"></i>Tambah Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <a class="btn btn-app" href="index.php?content=kelahiran/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <form action="index.php?content=kelahiran/save" method="post" enctype="multipart/form-data">
                        <h3>Tambah Data Kelahiran Hewan Ternak</h3>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID Kelahiran</th>
                                <td width="1%">:</td>
                                <td>
                                    <?php
                                    $sqli = mysqli_query($config, "select * from kelahiran order by id_kelahiran DESC LIMIT 0,1");
                                    $data = mysqli_fetch_array($sqli);
                                    if ($data != null) {
                                        $kodeawal = substr($data['id_kelahiran'], 3, 4) + 1;
                                    } else {
                                        $kodeawal = 1;
                                    }
                                    if ($kodeawal < 10) {
                                        $kode = 'L-00' . $kodeawal;
                                    } elseif ($kodeawal > 9 && $kodeawal <= 99) {
                                        $kode = 'L-0' . $kodeawal;
                                    } else {
                                        $kode = 'L-' . $kodeawal;
                                    }
                                    ?>
                                    <input type="text" class="form-control" value="<?php echo $kode; ?>" name="id_kelahiran" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">ID Hewan</th>
                                <td width="1%">:</td>
                                <td>
                                    <?php
                                    // $sqli = mysqli_query($config, "select * from hewan order by id_hewan DESC LIMIT 0,1");
                                    // $data = mysqli_fetch_array($sqli);
                                    // if ($data != null) {
                                    //     $kodeawal = substr($data['id_hewan'], 4, 4) + 1;
                                    // } else {
                                    //     $kodeawal = 1;
                                    // }
                                    // if ($kodeawal < 10) {
                                    //     $kode = 'SP-00' . $kodeawal;
                                    // } elseif ($kodeawal > 9 && $kodeawal <= 99) {
                                    //     $kode = 'SP-0' . $kodeawal;
                                    // } else {
                                    //     $kode = 'SP-' . $kodeawal;
                                    // }
                                    ?>
                                    <input type="text" class="form-control" value="<?php /*echo $kode;*/ ?>" name="id_hewan">
                                </td>
                            </tr>
                            <tr>
                                <th>ID Ibu</th>
                                <td>:</td>
                                <td>
                                    <select class="form-control selectpicker" name="id_ibu" required>
                                        <option value="" selected disabled>- pilih -</option>
                                        <?php
                                        $sql = mysqli_query($config, "select * from hewan where jk='Betina'");
                                        $arrayjs = "var varid = new Array();\n";

                                        while ($data1 = mysqli_fetch_array($sql)) {
                                            echo "<option value='$data1[id_hewan]'> $data1[id_hewan] </option>";

                                            $arrayjs .= "var varid['" . $data1['id_hewan'] . "']= 
                                            {vid:'" . addslashes($data1['id_hewan']) . "'};\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>ID Bapa</th>
                                <td>:</td>
                                <td>
                                    <select class="form-control selectpicker" name="id_bapa" required>
                                        <option value="" selected disabled>- pilih -</option>
                                        <?php
                                        $sql = mysqli_query($config, "select * from hewan where jk='Jantan'");
                                        $arrayjs = "var varid = new Array();";

                                        while ($data2 = mysqli_fetch_array($sql)) {
                                            echo "<option value='$data2[id_hewan]'> $data2[id_hewan] </option>";

                                            $arrayjs .= "var varid['" . $data2['id_hewan'] . "']= 
                                            {vid:'" . addslashes($data2['id_hewan']) . "'};\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Lahir</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" name="tgl_lahir" required></td>
                            </tr>

                            <tr>
                                <th>Bobot</th>
                                <td>:</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" name="bobot" required onkeyup="validAngka(this)" class="form-control">
                                        <span class="input-group-addon">Kg.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Tinggi</th>
                                <td>:</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" name="tinggi" required onkeyup="validAngka(this)" class="form-control">
                                        <span class="input-group-addon">Cm.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td>

                                    <label>
                                        <input type="radio" name="jk" class="minimal" value="Jantan"> Jantan
                                    </label>
                                    <label>
                                        <input type="radio" name="jk" class="minimal" value="Betina"> Betina

                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Jenis Sapi</th>
                                <td width="1%">:</td>
                                <td>
                                    <select class="form-control selectpicker" name="jenis_sapi" required>
                                        <option value="" selected disabled>- Pilih Jenis Sapi -</option>
                                        <option value="Simental Cross">Simental Cross</option>
                                        <option value="Limosin Cross">Limosin Cross</option>
                                        <option value="PO">PO</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Foto</th>
                                <td width="1%">:</td>
                                <td>
                                    <input type="file" class="form-control" name="foto" id="foto" required>
                                    <p class="help-block">Pilih file jpg atau png.</p>
                                </td>
                            </tr>

                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->