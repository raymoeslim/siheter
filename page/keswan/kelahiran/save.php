<script src="../../../assets/sweetalert/sweetalert2.all.min.js"></script>
<?php

$id_kelahiran = $_POST['id_kelahiran'];
$id_hewan = $_POST['id_hewan'];
$id_ibu = $_POST['id_ibu'];
$id_bapa = $_POST['id_bapa'];
$tgl_lahir = $_POST['tgl_lahir'];
$bobot = $_POST['bobot'];
$tinggi = $_POST['tinggi'];
$jk = $_POST['jk'];
$jenis_sapi = $_POST['jenis_sapi'];

$filename = $_FILES['foto']['name'];
$move = move_uploaded_file($_FILES['foto']['tmp_name'], '../../assets/images/hewan/' . $filename);

$query = mysqli_query($config, "insert into kelahiran (id_kelahiran, id_hewan, id_ibu, id_bapa, tgl_lahir, bobot, tinggi, jenis_sapi) values ('$id_kelahiran','$id_hewan','$id_ibu','$id_bapa','$tgl_lahir','$bobot','$tinggi','$jenis_sapi')");
$query_hewan = mysqli_query($config, "insert into hewan (id_hewan, tgl_lahir, jk, jenis_sapi, foto) values ('$id_hewan','$tgl_lahir', '$jk', '$jenis_sapi', '$filename')");

if ($query && $query_hewan) {
    //echo mysqli_error();
    echo "
		<script type='text/javascript'>
		    setTimeout(function () {
			    Swal.fire({
				    icon: 'success',
				    title: 'Data Kelahiran Hewan Tersimpan!',
				    timer: 1700,
				    showConfirmButton: true
			    });
		    },10);
		    window.setTimeout(function(){
			window.location.replace('index.php?content=kelahiran/get');
		    } ,1500);
		</script>";
} else {
    //echo mysqli_error($config);
    echo "
    <script type='text/javascript'>
    setTimeout(function () {
        Swal.fire({
            icon: 'error',
            title: 'Data Kelahiran Hewan Gagal Tersimpan!',
            timer: 1700,
            showConfirmButton: true
        });
    },10);
    window.setTimeout(function(){
        history.back();
    } ,1500);
    </script>";
}
?>