<section class="content-header">
  <h1>
    Dashboard
    <small>Sistem Informasi Hewan Ternak Sapi</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->

  <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Login Berhasil! <i class="icon fa fa-check"></i></h4>
    <p>Selamat Bekerja <b>Keswan</b>.</p>
  </div>

  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php
            $sql = mysqli_query($config, "select * from kelahiran");
            $jml_data = mysqli_num_rows($sql);
            if ($jml_data > 0) {
              echo "<b>$jml_data</b>";
            } else {
              echo "0";
            }
            ?></h3>

        <p>Data Kelahiran Hewan</p>
      </div>
      <div class="icon">
        <i class="ion ion-heart"></i>
      </div>
      <a href="index.php?content=kelahiran/get" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3>
          <?php
          $sql = mysqli_query($config, "select * from kesehatan");
          $jml_data = mysqli_num_rows($sql);
          if ($jml_data > 0) {
            echo "<b>$jml_data</b>";
          } else {
            echo "0";
          }
          ?>
        </h3>

        <p>Data Kesehatan Hewan</p>
      </div>
      <div class="icon">
        <i class="ion ion-medkit"></i>
      </div>
      <a href="index.php?content=kesehatan/get" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-blue">
      <div class="inner">
        <h3>
          <?php
          $sql = mysqli_query($config, "select * from ib");
          $jml_data = mysqli_num_rows($sql);
          if ($jml_data > 0) {
            echo "<b>$jml_data</b>";
          } else {
            echo "0";
          }
          ?>
        </h3>

        <p>Data IB Hewan </p>
      </div>
      <div class="icon">
        <i class="fa fa-certificate"></i>
      </div>
      <a href="index.php?content=ib/get" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-purple">
      <div class="inner">
        <h3>
          <?php
          $sql = mysqli_query($config, "select * from pkb");
          $jml_data = mysqli_num_rows($sql);
          if ($jml_data > 0) {
            echo "<b>$jml_data</b>";
          } else {
            echo "0";
          }
          ?>
        </h3>

        <p>Data PKB Hewan </p>
      </div>
      <div class="icon">
        <i class="fa fa-dot-circle-o"></i>
      </div>
      <a href="index.php?content=pkb/get" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  </div>
  <!-- /.row -->
  <!-- Main row -->

  <!-- /.nav-tabs-custom -->
</section>