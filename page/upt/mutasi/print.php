<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html moznomarginboxes mozdisallowselectionprint>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="sourcut icon" href="../../../../../../../assets/dist/img/logo.png">
    <script language="javascript1.2">
        function printpage() {
            window.print();
        }
    </script>
</head>

<body onload="printpage()">
    <div style="color: #000;" align="center">
        <img src="../../../assets/Images/site/logo.png" alt="logo" height="100">
        <h3>Sistem Informasi Hewan Ternak Sapi</h3>
        <p><strong>UPTD BPPPT Majalengka</strong></p>
    </div>
    <table border="1" align="center" style="border-collapse:collapse;">
        <th colspan="9">Data Mutasi Hewan Ternak</th>
        <tr>
            <th>No</th>
            <th>ID Mutasi</th>
            <th>Status</th>
            <th>Tanggal Mutasi</th>
            <th>ID Hewan</th>
            <th>Keterangan</th>
            <th>Foto</th>
        </tr>
        <?php
        include "../../../config/connection.php";
        $i = 1;
        $query = mysqli_query($config, "select * from mutasi");
        while ($data = mysqli_fetch_array($query)) {
            echo "<tr>
                	<td>$i.</td>
                    <td>$data[id_mutasi]</td>
                    <td>$data[status]</td>
                    <td>$data[tgl]</td>
                    <td>$data[id_hewan]</td>
                    <td>$data[ket]</td>
                    <td><img src='../../../assets/images/mutasi/$data[foto]' width='100' height='70'/></td>
                  </tr>";
            $i = $i + 1;
        }
        ?>
    </table>
</body>

</html>