<section class="content-header">
    <h1>
        Edit Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-edit"></i>Edit Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=mutasi/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <form action="index.php?content=mutasi/update" method="post" enctype="multipart/form-data">
                        <h3>Edit Data Mutasi Hewan</h3>

                        <?php
                        $id_mutas = $_REQUEST['id_mutasi'];
                        $data = mysqli_fetch_array(mysqli_query($config, "select * from mutasi where id_mutasi='$id_mutas'"));
                        ?>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID Mutasi</th>
                                <td width="1%">:</td>
                                <td>
                                    <input type="text" class="form-control" value="<?php echo $data['id_mutasi']; ?>" name="id_mutasi" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th>ID Hewan</th>
                                <td>:</td>
                                <td>
                                    <input type="text" class="form-control" value="<?php echo $data['id_hewan']; ?>" name="id_hewan" readonly>
                                </td>

                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Mutasi</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" value="<?php echo $data['tgl']; ?>" name="tgl" required></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>:</td>
                                <td>

                                    <select class="form-control" name="status" style="width: 100%;">

                                        <option value="-" <?php if ($data['status'] == "-") {
                                                                echo "selected";
                                                            } ?>>- Pilih Status -</option>
                                        <option value="Terjual" <?php if ($data['status'] == "Terjual") {
                                                                    echo "selected";
                                                                } ?>>Terjual</option>
                                        <option value="Mati" <?php if ($data['status'] == "Mati") {
                                                                    echo "selected";
                                                                } ?>>Mati</option>


                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td>:</td>
                                <td><textarea class="form-control" name="ket" required><?php echo $data['ket']; ?></textarea></td>
                            </tr>
                            <tr>
                                <th width="20%">Foto</th>
                                <td width="1%">:</td>
                                <td><?php
                                    echo "<img src='../../assets/images/mutasi/$data[foto]' class='foto'/>";
                                    ?></td>
                            </tr>
                            <tr>
                                <th width="20%">*Pilih Foto</th>
                                <td width="1%">:</td>
                                <td>
                                    <input type="file" class="form-control" name="foto" id="foto">
                                    <p class="help-block">Pilih file jpg atau png.</p>
                                    <p class="help">*Jangan <b>Pilih Foto</b> apabila tidak akan mengganti foto.</p>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Update
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->