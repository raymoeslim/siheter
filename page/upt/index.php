<?php
include "../../config/connection.php";
include "../../config/security.php";

?>
<!DOCTYPE html>
<html>


<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Informasi Hewan Ternak Sapi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../assets/dist/css/skins/_all-skins.min.css">
  <link rel="sourcut icon" href="../../assets/dist/img/logo.png">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    .foto {
      width: 140px;
      height: 90px;
      border: #0000FF 1px solid;
      background-size: cover;
      cursor: pointer;
    }

    .foto:hover {
      transition: all 0.3s;
      transform: scale(1.9);
      -moz-transition: all 0.3s;
      -moz-transform: scale(1.9);
    }
  </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">SI<b>HT</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">UPT <b>SIHETER</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
              <a href="../../config/auth-logout.php" id="tombol" onclick="return confirm('Apakah anda yakin ingin logout?')">
                <i class="fa fa-sign-out"> Logout</i>
              </a>
            </li>

          </ul>
        </div>
      </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->


        <!-- sidebar menu: : style can be found in sidebar.less -->

        <?php
        function is_active($page)
        {
          $uri = "$_SERVER[REQUEST_URI]";
          if (strpos($uri, $page)) {
            echo 'active';
          }
        }
        ?>
        <ul class="sidebar-menu">
          <li class="header">MAIN NAVIGATION</li>
          <li class="<?php is_active('dashboard'); ?>">
            <a href="index.php?content=dashboard/dashboard">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>

          <li class="<?php is_active('hewan'); ?>">
            <a href="index.php?content=hewan/get">
              <i class="fa fa-list"></i><span>Data Hewan Ternak</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>


          <li class="<?php is_active('kelahiran'); ?>">
            <a href="index.php?content=kelahiran/get">
              <i class="fa fa-heartbeat"></i><span>Data Kelahiran</span>
              <span class="pull-right-container">

              </span>
            </a>
          </li>

          <li class="<?php is_active('kesehatan'); ?>">
            <a href="index.php?content=kesehatan/get">
              <i class="fa fa-medkit"></i><span>Data Kesehatan</span>
              <span class="pull-right-container">
              </span>
            </a>
          </li>

          <li class="<?php is_active('mutasi'); ?>">
            <a href="index.php?content=mutasi/get">
              <i class="fa fa-random"></i><span>Data Mutasi Hewan</span>
              <span class="pull-right-container">

              </span>
            </a>
          </li>

          <li class="<?php is_active('ib'); ?>">
            <a href="index.php?content=ib/get">
              <i class="fa fa-certificate"></i><span>Data IB</span>
              <span class="pull-right-container">

              </span>
            </a>
          </li>

          <li class="<?php is_active('pkb'); ?>">
            <a href="index.php?content=pkb/get">
              <i class="fa fa-dot-circle-o"></i><span>Data PKB</span>
              <span class="pull-right-container">

              </span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->



    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Content  -->

      <?php
      if (!isset($_GET['content'])) {
        include "dashboard/dashboard.php";
      } else {
        $content = $_GET['content'];
        include $content . ".php";
      }
      ?>

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="pull-right hidden-xs">

      </div>
      <strong>Copyright &copy; 2022 <a href="">Restika Septiani 18.14.1.0052</a>.</strong> All rights
      reserved.
    </footer>
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="../../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- bootstrap datepicker -->
  <script src="../../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- SlimScroll -->
  <script src="../../assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="../../assets/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="../../assets/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../assets/dist/js/demo.js"></script>

  <script src="../../assets/dist/sweetalert/sweetalert2.all.min.js"></script>

  <!-- page script -->
  <!-- page script -->
  <script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    function validAngka(a) {
      if (!/^[0-9.]+$/.test(a.value)) {
        a.value = a.value.substring(0, a.value.lenght - 1000);
      }
    }

    function validHuruf(a) {
      if (!/^[a-zA-Z ]+$/.test(a.value)) {
        a.value = a.value.substring(0, a.value.lenght - 1000);
      }
    }
  </script>

  <!-- Datatable -->
  <script src="../../assets/plugins/datatables/jquery.dataTables.min.js" charset="utf-8"></script>
  <script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#datatable').DataTable({
        "language": {
          "lengthMenu": "Menampilkan _MENU_ data per halaman",
          "zeroRecords": "Tidak ada Data !",
          "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
          "infoEmpty": "Data tidak tersedia !",
          "search": "Pencarian :",
          "next": "Selanjutnya",
          "previous": "Sebelumnya",
        },
        ordering: false,
      });
    });
  </script>

</body>

</html>