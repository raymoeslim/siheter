<section class="content-header">
    <h1>
        Edit Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-edit"></i>Edit Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=kelahiran/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <form action="index.php?content=kelahiran/update" method="post" enctype="multipart/form-data">
                        <h3>Edit Data Kelahiran Hewan Ternak</h3>

                        <?php
                        $id_kelahiran = $_REQUEST['id_kelahiran'];
                        $data = mysqli_fetch_array(mysqli_query($config, "select * from kelahiran where id_kelahiran='$id_kelahiran'"));
                        ?>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID Kelahiran</th>
                                <td width="1%">:</td>
                                <td>
                                    <input type="text" class="form-control" value="<?php echo $data['id_kelahiran']; ?>" name=" id_kelahiran" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">ID Hewan</th>
                                <td width="1%">:</td>
                                <td><input type="text" class="form-control" value="<?php echo $data['id_hewan']; ?>" name="id_hewan" readonly></td>
                            </tr>
                            <tr>
                                <th>ID Ibu</th>
                                <td>:</td>
                                <td><input type="text" class="form-control" value="<?php echo $data['id_ibu']; ?>" name="id_ibu" readonly></td>
                            </tr>
                            <tr>
                                <th>ID Bapa</th>
                                <td>:</td>
                                <td><input type="text" class="form-control" value="<?php echo $data['id_bapa']; ?>" name="id_bapa" readonly></td>

                            </tr>
                            <tr>
                                <th width="20%">Tanggal Lahir</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" value="<?php echo $data['tgl_lahir']; ?>" name="tgl_lahir" required></td>
                            </tr>
                            <tr>
                                <th>Bobot</th>
                                <td>:</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" name="bobot" required value="<?php echo $data['bobot']; ?>" onkeyup="validAngka(this)" class="form-control">
                                        <span class="input-group-addon">Kg.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Tinggi</th>
                                <td>:</td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" name="tinggi" value="<?php echo $data['tinggi']; ?>" required onkeyup="validAngka(this)" class="form-control">
                                        <span class="input-group-addon">Cm.</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Jenis Sapi</th>
                                <td width="1%">:</td>
                                <td>
                                    <select class="form-control" name="jenis_sapi" style="width: 100%;">

                                        <option value="-" <?php if ($data['jenis_sapi'] == "-") {
                                                                echo "selected";
                                                            } ?>>- Pilih Jenis Sapi -</option>

                                        <option value="Simental Cross" <?php if ($data['jenis_sapi'] == "Simental Cross") {
                                                                            echo "selected";
                                                                        } ?>>Simental Cross</option>
                                        <option value="Limosin Cross" <?php if ($data['jenis_sapi'] == "Limosin Cross") {
                                                                            echo "selected";
                                                                        } ?>>Limosin Cross</option>
                                        <option value="PO" <?php if ($data['jenis_sapi'] == "PO") {
                                                                echo "selected";
                                                            } ?>>PO</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->