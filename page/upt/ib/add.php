<section class="content-header">
    <h1>
        Tambah Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-plus"></i>Tambah Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <a class="btn btn-app" href="index.php?content=ib/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <form action="index.php?content=ib/save" method="post" enctype="multipart/form-data">
                        <h3>Tambah Data IB Hewan</h3>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID IB</th>
                                <td width="1%">:</td>
                                <td>
                                    <?php
                                    $sqli = mysqli_query($config, "select * from ib order by id_ib DESC LIMIT 0,1");
                                    $data = mysqli_fetch_array($sqli);
                                    if ($data != null) {
                                        $kodeawal = substr($data['id_ib'], 3, 4) + 1;
                                    } else {
                                        $kodeawal = 1;
                                    }
                                    if ($kodeawal < 10) {
                                        $kode = 'IB-00' . $kodeawal;
                                    } elseif ($kodeawal > 9 && $kodeawal <= 99) {
                                        $kode = 'IB-0' . $kodeawal;
                                    } else {
                                        $kode = 'IB-' . $kodeawal;
                                    }
                                    ?>
                                    <input type="text" class="form-control" value="<?php echo $kode; ?>" name="id_ib" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th>ID Hewan</th>
                                <td>:</td>
                                <td>
                                    <select class="form-control selectpicker" name="id_hewan" required>
                                        <option value="" selected disabled>- Pilih ID Hewan -</option>
                                        <?php include "../config/koneksi.php";
                                        $sql = mysqli_query($config, "select * from hewan");
                                        $arrayjs = "var varid = new Array();\n";

                                        while ($data1 = mysqli_fetch_array($sql)) {
                                            echo "<option value='$data1[id_hewan]'> $data1[id_hewan] </option>";

                                            $arrayjs .= "var varid['" . $data1['id_hewan'] . "']= 
                                            {vid:'" . addslashes($data1['id_hewan']) . "'};\n";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal ib</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" name="tgl" required></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>:</td>
                                <td>
                                    <select class="form-control selectpicker" name="status" required>
                                        <option value="" selected disabled>- Pilih Status -</option>
                                        <option value="Berhasil">Berhasil</option>
                                        <option value="Gagal">Gagal</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td>:</td>
                                <td><textarea class="form-control" name="ket" required></textarea></td>
                            </tr>

                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->