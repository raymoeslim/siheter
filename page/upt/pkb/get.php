<section class="content-header">
    <h1>
        Data PKB Hewan Ternak
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-dot-circle-o"></i>Data PKB Hewan Ternak</a></li>

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=pkb/add">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                    <a class="btn btn-app" href="pkb/print.php" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-striped table-condensed table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID PKB</th>
                                <th>ID Hewan</th>
                                <th>Tanggal PKB</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 1;
                            $query = mysqli_query($config, "select * from pkb");
                            while ($data = mysqli_fetch_array($query)) {
                                $today = date("Y-m-d");
                                $diff = date_diff(date_create($data['tgl']), date_create($today));
                                echo "<tr>
                                        <td>$i</td>
                                        <td>$data[id_pkb]</td>
                                        <td>$data[id_hewan]</td>
                                        <td>$data[tgl] | " . $diff->format('%y Tahun, %M Bulan, %D Hari yang lalu') . "</td>
                                        <td>$data[status]</td>
                                        <td>$data[ket]</td>
                                        <td>
                                            <a href='index.php?content=pkb/delete&id_pkb=$data[id_pkb]' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</a>
                                            <a href='index.php?content=pkb/edit&id_pkb=$data[id_pkb]' class='btn btn-info'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                        
                                        </td>
                                    </tr>";
                                $i = $i + 1;
                            }
                            ?>

                        </tbody>
                    </table>

                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->


        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->