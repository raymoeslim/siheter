<section class="content-header">
    <h1>
        Edit Data
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="#"><i class="fa fa-edit"></i>Edit Data</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=hewan/get">
                        <i class="fa fa-reply"></i> Kembali
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                    <?php
                    $id_hewan = $_REQUEST['id_hewan'];
                    $data = mysqli_fetch_array(mysqli_query($config, "select * from hewan where id_hewan='$id_hewan'"));
                    ?>

                    <form action="index.php?content=hewan/update" method="post" enctype="multipart/form-data">
                        <h3>Edit Data Hewan Ternak</h3>

                        <table class="table table-striped table-middle">
                            <tr>
                                <th width="20%">ID Hewan</th>
                                <td width="1%">:</td>
                                <td><input type="text" class="form-control" name="id_hewan" value="SP-<?php echo $data['id_hewan']; ?>" readonly onkeyup="validAngka(this)"></td>
                            </tr>
                            <tr>
                                <th width="20%">Tanggal Lahir</th>
                                <td width="1%">:</td>
                                <td><input type="date" class="form-control" name="tgl_lahir" required value="<?php echo $data['tgl_lahir']; ?>"></td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td>
                                    <?php
                                    if ($data['jk'] == 'Jantan') {
                                    ?>
                                        <label>
                                            <input type="radio" name="jk" class="minimal" value="Jantan" checked="checked"> Jantan
                                        </label>

                                        <label>
                                            <input type="radio" name="jk" class="minimal" value="Betina"> Betina
                                        </label>
                                    <?php } else {
                                    ?>
                                        <label>
                                            <input type="radio" name="jk" class="minimal" value="Jantan"> Jantan
                                        </label>
                                        <label>
                                            <input type="radio" name="jk" class="minimal" value="Betina" checked="checked"> Betina
                                        </label>

                                    <?php
                                    } ?>

                                </td>
                            </tr>

                            <tr>
                                <th width="20%">Jenis Sapi</th>
                                <td width="1%">:</td>
                                <td>
                                    <select class="form-control" name="jenis_sapi" style="width: 100%;">

                                        <option value="-" <?php if ($data['jenis_sapi'] == "-") {
                                                                echo "selected";
                                                            } ?>>- Pilih Jenis Sapi -</option>

                                        <option value="Simental Cross" <?php if ($data['jenis_sapi'] == "Simental Cross") {
                                                                            echo "selected";
                                                                        } ?>>Simental Cross</option>
                                        <option value="Limosin Cross" <?php if ($data['jenis_sapi'] == "Limosin Cross") {
                                                                            echo "selected";
                                                                        } ?>>Limosin Cross</option>
                                        <option value="PO" <?php if ($data['jenis_sapi'] == "PO") {
                                                                echo "selected";
                                                            } ?>>PO</option>

                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <th width="20%">Foto</th>
                                <td width="1%">:</td>
                                <td><?php
                                    echo "<img src='../../assets/images/hewan/$data[foto]' class='foto'/>";
                                    ?></td>
                            </tr>

                            <tr>
                                <th width="20%">*Pilih Foto</th>
                                <td width="1%">:</td>
                                <td>
                                    <input type="file" class="form-control" name="foto" id="foto">
                                    <p class="help-block">Pilih file jpg atau png.</p>
                                    <p class="help">*Jangan <b>Pilih Foto</b> apabila tidak akan mengganti foto.</p>
                                </td>
                            </tr>

                        </table>
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i> Update
                        </button>
                        <button type="button" class="btn btn-danger" onclick="javascript:history.back();">
                            <i class="fa fa-arrow-circle-left"></i> Batal
                        </button>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->