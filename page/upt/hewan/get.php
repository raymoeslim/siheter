<section class="content-header">
    <h1>
        Data Hewan Ternak
        <small>Sistem Informasi Hewan Ternak Sapi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#"><i class="fa fa-th-list"></i>Data Hewan Ternak</a></li>

    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">

                    <a class="btn btn-app" href="index.php?content=hewan/add">
                        <i class="fa fa-plus"></i> Tambah Data
                    </a>
                    <a class="btn btn-app" href="">
                        <i class="fa fa-refresh"></i> Refresh
                    </a>
                    <a class="btn btn-app" href="hewan/print.php" target="_blank">
                        <i class="fa fa-print"></i> Cetak
                    </a>
                </div>
            </div>

            <!-- /.box -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">

                    <table class="table table-striped table-condensed table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ID Hewan</th>
                                <th>Jenis Sapi</th>
                                <th>Jenis Kelamin</th>
                                <th>Tanggal Lahir</th>
                                <th>Umur</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $i = 1;
                            $query = mysqli_query($config, "select * from hewan");
                            while ($data = mysqli_fetch_array($query)) {
                                $today = date("Y-m-d");
                                $diff = date_diff(date_create($data['tgl_lahir']), date_create($today));
                                echo "<tr>
                                        <td>$i</td>
                                        <td>$data[id_hewan]</td>
                                        <td>$data[jenis_sapi]</td>
                                        <td>$data[jk]</td>
                                        <td>$data[tgl_lahir]</td>
                                        <td>" . $diff->format('%y Tahun, %M Bulan, %D Hari') . "</td>
                                        <td><img src='../../assets/images/hewan/$data[foto]' class='foto'/></td>
                                        <td>
                                            <a href='index.php?content=hewan/delete&id_hewan=$data[id_hewan]' class='btn btn-danger'><i class='glyphicon glyphicon-trash'></i> Delete</a>
                                            <a href='index.php?content=hewan/edit&id_hewan=$data[id_hewan]' class='btn btn-info'><i class='glyphicon glyphicon-edit'></i> Edit</a>
                                            <a href='hewan/qrcode.php?id_hewan=$data[id_hewan]' target='_blank' class='btn btn-warning'><i class='fa fa-qrcode'></i> QR</a>
                                        </td>
                                    </tr>";
                                $i = $i + 1;
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->