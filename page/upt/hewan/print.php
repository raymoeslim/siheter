<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html moznomarginboxes mozdisallowselectionprint>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="sourcut icon" href="../../../../../../../assets/dist/img/logo.png">
    <script language="javascript1.2">
        function printpage() {
            window.print();
        }
    </script>
</head>

<body onload="printpage()">
    <div style="color: #000;" align="center">
        <img src="../../../assets/images/site/logo.png" alt="logo" height="100">
        <h3>Sistem Informasi Hewan Ternak Sapi</h3>
        <p><strong>UPTD BPPPT Majalengka</strong></p>
    </div>
    <table border="1" align="center" style="border-collapse:collapse;">
        <th colspan="8" align="center">Data Hewan Ternak</th>
        <tr>
            <th>No.</th>
            <th>ID Hewan</th>
            <th>Foto</th>
            <th>Jenis Sapi</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Umur</th>
        </tr>
        <?php
        include "../../../config/connection.php";
        $i = 1;
        $query = mysqli_query($config, "select * from hewan");
        while ($data = mysqli_fetch_array($query)) {
            $today = date("Y-m-d");
            $diff = date_diff(date_create($data['tgl_lahir']), date_create($today));
            echo "<tr>
                    <td>$i</td>
                    <td>SP-$data[id_hewan]</td>
                    <td><img src='../../../assets/images/hewan/$data[foto]' width='100' height='70'/></td>
                    <td>$data[jenis_sapi]</td>
                    <td>$data[jk]</td>
                    <td>$data[tgl_lahir]</td>
                    <td>" . $diff->format('%y Tahun, %M Bulan, %D Hari') . "</td>
                </tr>";
            $i = $i + 1;
        }
        ?>
    </table>
</body>

</html>