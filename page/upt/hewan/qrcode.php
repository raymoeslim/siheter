<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html moznomarginboxes mozdisallowselectionprint>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <link rel="sourcut icon" href="../../../../../../../assets/dist/img/logo.png">
  <script language="javascript1.2">
    function printpage() {
      window.print();
    }
  </script>
</head>

<body onload="printpage()">
  <div style="color: #000;" align="center">
    <img src="../../../assets/images/site/logo.png" alt="logo" height="100">
    <h3>Sistem Informasi Hewan Ternak Sapi</h3>
    <p><strong>UPTD BPPPT Majalengka</strong></p>
  </div>
  <?php
  include '../../../assets/library/phpqrcode/qrlib.php';

  $id_hewan = $_REQUEST['id_hewan'];
  $code = "index.php?content=kesehatan/qr-add&id_hewan=" . $id_hewan;
  $encrypt = base64_encode($code);

  array_map('unlink', array_filter((array) glob("../../../assets/images/temp/*")));

  QRcode::png($encrypt, "../../../assets/images/temp/" . $id_hewan . ".png", QR_ECLEVEL_Q, 3);

  echo '<center>
    <img src="../../../assets/images/temp/' . $id_hewan . '.png" />
    <br>ID Hewan : ' . $id_hewan . '
    </center>';
  ?>

</body>

</html>