<?php
session_start();
if (!isset($_SESSION['username']) || !isset($_SESSION['role'])) {
  // jika user belum login
  header('Location: ../../index.php');
  exit();
}

$uri = "$_SERVER[REQUEST_URI]";
if (strpos($uri, "admin")) {
  if ($_SESSION['role'] != 1) {
    // jika user belum login
    header('Location: ../../index.php');
    exit();
  }
}

if (strpos($uri, "keswan")) {
  if ($_SESSION['role'] != 2) {
    // jika user belum login
    header('Location: ../../index.php');
    exit();
  }
}

if (strpos($uri, "upt")) {
  if ($_SESSION['role'] != 3) {
    // jika user belum login
    header('Location: ../../index.php');
    exit();
  }
}
